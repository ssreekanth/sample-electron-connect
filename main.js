var app = require('app'),
    BrowserWindow = require('browser-window'),
    ipc = require('ipc');

var mainWindow = null;
var childWindow = null;

app.commandLine.appendSwitch('v', -1);
app.commandLine.appendSwitch('vmodule', 'console=0');

app.on('window-all-closed', function() {
    if (process.platform != 'darwin') {
        app.quit();
    }
});

app.on('ready', function() {
    mainWindow = new BrowserWindow({
        width: 480,
        height: 640,
        frame: false,
        resizable: true
    });
    mainWindow.loadUrl('file://' + __dirname + '/gui/main.html');

    mainWindow.on('closed', function() {
        mainWindow = null;
    });

    ipc.on('child:open', function(event) {
        console.log('main.js :: ipc msg child:open received');
        openChildWindow();
    });

    openChildWindow = function() {
        if (!!childWindow) {
            console.log('main.js :: child window already open, sending msg to it');
            childWindow.webContents.send('url', "http://www.google.com");
        } else {
            console.log('main.js :: create a new child window');
            childWindow = new BrowserWindow({
                width: 1080,
                height: 640,
                "overlay-fullscreen-video": true
            });
            childWindow.loadUrl('file://' + __dirname + '/gui/child.html');

            childWindow.webContents.on('dom-ready', function() {
                console.log('main.js :: new child window loaded. sending url to the child window.');
                childWindow.webContents.send('url', "http://domain.tld");
                childWindow.show();
            });

            childWindow.on('closed', function() {
                console.log('main.js :: child window closed');
                childWindow = null;
            });
        }
    };
});
