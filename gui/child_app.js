angular.module('ChildApp', ['ionic'])
  .config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    $ionicConfigProvider.views.transition('none');
    $stateProvider
      .state('webview', {
        url: "/webview",
        templateUrl: "templates/webview.html",
        controller: "WebViewController"
      });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/webview');
  })
  .factory('NodeService', function() {
    var ipc = require('ipc');
    return {
      ipc: ipc,
    };
  })
  .controller('WebViewController', function($scope, NodeService) {

    var webview = document.getElementById('content');

    $scope.reload = function() {
      webview.reload();
    };

    NodeService.ipc.on('url', function(url) {
      console.log('child :: url received: ' + url);
      webview.src = url;
    });

    webview.addEventListener('did-fail-load', function(e) {
      console.log('child :: failed to load - ' + webview.src);
      console.log('e : ' + JSON.stringify(e));
    });

    webview.addEventListener('did-finish-load', function() {
      console.log('child :: finished loading - ' + webview.src);
    });

    webview.addEventListener('dom-ready', function() {
      console.log('child :: dom-ready fired - ' + webview.src);
    });

    webview.addEventListener('did-get-response-details', function() {
      console.log('child :: got response - ' + webview.src);
    });

    webview.addEventListener('ipc-message', function(e) {
      console.log(e.channel);
      if (e.channel === 'mouseover-href') {
        console.log('child :: href: ' + e.args[0]);
      }
    });
  });