angular.module('App', ['ionic'])
  .config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    $ionicConfigProvider.views.transition('none');
    $stateProvider
      .state('list', {
        url: "/list",
        templateUrl: "templates/list.html",
        controller: "ListController"
      });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/list');
  })
  .factory('NodeService', function() {
      var ipc = require('ipc');
      return {
          ipc: ipc,
      };
  })
  .controller('ListController', function($scope, NodeService) {

    $scope.openChild = function() {
      console.log('main :: open child button clicked. sending child:open ipc msg to main process');
      NodeService.ipc.send('child:open');
    };
  });
