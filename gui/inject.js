var ipc = require('ipc');
document.addEventListener('mouseover', function(event) {
    var hoveredEl = event.target;
    if (hoveredEl.tagName !== 'A') { return; }
    ipc.sendToHost('mouseover-href', hoveredEl.href);
});
